<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_6843 testyzTestLinka</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>9a4823cf-d3ec-4129-9e4b-7e7d85114fab</testSuiteGuid>
   <testCaseLink>
      <guid>71fe3e8c-81c4-48a5-b439-3f5286adf5e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestLink/TC_6843Dodawanie produktu do koszyka</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63f2280a-e32d-4c5b-8bdb-a44321eac8e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestLink/TC_6843Logowanie z użyciem błędnych danych</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7692a637-4989-4f4e-86f1-d87b52fdae3f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestLink/TC_6843Logowanie z użyciem poprawnych danych</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
